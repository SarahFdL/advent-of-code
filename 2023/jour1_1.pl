#!/usr/perl
use strict;
use warnings;

open(my $fic,'<',"input_1.txt") or die("ouverture : $!");

# entrée : tableau de cdc
# sortie : entier
#
# pour chaque ligne du fichier 
# regex 
# additionner les deux dans res -> multiplier le 1er par 10 et ajouter le 2e pour l'unité
# sortir res

sub calibration {
	my $res = 0 ;
	my $ligne ;

	while (defined($ligne = <$fic>)) {
		chomp $ligne ;
		if ($ligne=~ m/\D*(\d).*(\d)\D*/) {
			$res += $1*10 + $2 ;
		} else {
			$ligne=~ m/\D*(\d)\D*/ ;
			$res += $1*10 + $1 ;
		}
	}

	print "$res\n";
}

calibration() ;

close ($fic);
