<?php

function contient_symbole($l_prec, $l, $l_suiv, $i, $lg) {
	$res = false ;
	for($j=1 ; $j < $lg-1 ; $j++) {
		if ((($l_prec[$j+$i-1] != '.') and (!is_numeric($l_prec[$j+$i-1])))
		or (($l_prec[$j+$i] != '.') and (!is_numeric($l_prec[$j+$i])))
		or (($l_prec[$j+$i+1] != '.') and (!is_numeric($l_prec[$j+$i+1])))
		or (($l_suiv[$j+$i-1] != '.') and (!is_numeric($l_suiv[$j+$i-1])))
		or (($l_suiv[$j+$i] != '.') and (!is_numeric($l_suiv[$j+$i])))
		or (($l_suiv[$j+$i+1] != '.') and (!is_numeric($l_suiv[$j+$i+1])))
		or (($l[$j+$i-1] != '.') and (!is_numeric($l[$j+$i-1])))
		or (($l[$j+$i+1] != '.') and (!is_numeric($l[$j+$i+1])))) {
			$res = true ;
		}
	}
	// dernier caractère
	if ((($l_prec[$j+$i-1] != '.') and (!is_numeric($l_prec[$j+$i-1])))
		or (($l_prec[$j+$i] != '.') and (!is_numeric($l_prec[$j+$i])))
		or (($l_prec[$j+$i+1] != '.') and (!is_numeric($l_prec[$j+$i+1])))
		or (($l_suiv[$j+$i-1] != '.') and (!is_numeric($l_suiv[$j+$i-1])))
		or (($l_suiv[$j+$i] != '.') and (!is_numeric($l_suiv[$j+$i])))
		or (($l_suiv[$j+$i+1] != '.') and (!is_numeric($l_suiv[$j+$i+1])))
		or (($l[$j+$i-1] != '.') and (!is_numeric($l[$j+$i-1])))) {
			$res = true ;
		}
	return $res ;
}

$fic = fopen('input_3.txt','r');
$ligne_prec = fgets($fic);
$longueur = strlen($ligne_prec);
$ligne_prec = str_split($ligne_prec) ;
$chiffre = '' ;
$ind_debut = -1 ;
$resultat = 0 ;
$symbole_adjacent = false ;

// traitement de la première ligne
$ligne = str_split(fgets($fic)) ; 
// premier caractère est un chiffre
if (is_numeric($ligne_prec[0])) {
	$chiffre = $ligne_prec[0] ;
	$ind_debut = 0 ;
}
// parcours de la première ligne
for ($i=1 ; $i<$longueur ; $i++) {
	echo "chiffre ".$chiffre." i ".$i." ligne[i] ".$ligne_prec[$i]."\n" ;
	// recherche d'un nombre dans la ligne
	while (is_numeric($ligne_prec[$i])) {
		echo "i : $i\n" ;
		$chiffre .= $ligne_prec[$i] ;
		if ($ind_debut == -1) { $ind_debut = $i ; }
		if ($i<$longueur) { $i++ ; } else { break ;} 
	}
	// recherche si symbole adjacent autour de ce nombre
	if ($chiffre != '') {
	echo "chiffre ".$chiffre." - ind debut ".$ind_debut."\n" ;
		// si le nombre commence en début de ligne
		if ($ind_debut == 0) {
			echo "dans if - ligne_prec[1]".$ligne_prec[1]." ligne[0] ".$ligne[0]." ligne[1] ".$ligne[1]."\n";
			if ((($ligne_prec[1] != '.') and (!is_numeric($ligne_prec[1])))
			or (($ligne[0] != '.') and (!is_numeric($ligne[0])))
			or (($ligne[1] != '.') and (!is_numeric($ligne[1])))) {
				echo "symbole adjacent\n";
				$symbole_adjacent = true ;
			}
		} else { // si le nombre est n'importe où
			echo "dans else - ligne_prec[ind_debut-1] ".$ligne_prec[$ind_debut-1]." linge_prec[ind_deb+1] ".$ligne_prec[$ind_debut+1].
				" ligne[ind_deb-1] ".$ligne[$ind_debut-1]." ligne[ind_deb] ".$ligne[$ind_debut]." ligne[ind_deb+1] ".$ligne[$ind_debut+1]."\n";
			if ((($ligne_prec[$ind_debut-1] != '.') and (!is_numeric($ligne_prec[$ind_debut-1])))
			or (($ligne_prec[$ind_debut+1] != '.') and (!is_numeric($ligne_prec[$ind_debut+1])))
			or (($ligne[$ind_debut-1] != '.') and (!is_numeric($ligne[$ind_debut-1])))
			or (($ligne[$ind_debut] != '.') and (!is_numeric($ligne[$ind_debut])))
			or (($ligne[$ind_debut+1] != '.') and (!is_numeric($ligne[$ind_debut+1])))) {
				$symbole_adjacent = true ;
				echo "symbole adjacent\n";
			}
		}
		for($j=1 ; $j < strlen($chiffre)-1 ; $j++) {
		echo "J ind_debut ".$ind_debut." j ".$j." - ligne_prec[j+ind_debut-1] ".$ligne_prec[$j+$ind_debut-1]."\n";
			if ((($ligne_prec[$j+$ind_debut-1] != '.') and (!is_numeric($ligne_prec[$j+$ind_debut-1])))
			or (($ligne_prec[$j+$ind_debut+1] != '.') and (!is_numeric($ligne_prec[$j+$ind_debut+1])))
			or (($ligne[$j+$ind_debut-1] != '.') and (!is_numeric($ligne[$j+$ind_debut-1])))
			or (($ligne[$j+$ind_debut] != '.') and (!is_numeric($ligne[$j+$ind_debut])))
			or (($ligne[$j+$ind_debut+1] != '.') and (!is_numeric($ligne[$j+$ind_debut+1])))) {
				$symbole_adjacent = true ;
				echo "symbole adjacent\n";
			}
		}
		// dernier caractère
		echo "dern car ind_debut ".$ind_debut." j ".$j." - ligne_prec[j+ind_debut-1] ".$ligne_prec[$j+$ind_debut-1].
			" ligne[j+ind_deb-1 ".$ligne[$j+$ind_debut-1]." ligne[j+ind_deb ".$ligne[$j+$ind_debut]."\n";
		if ($j != $longueur) {
			echo "J ind_debut ".$ind_debut." j ".$j." - ligne_prec[j+ind_debut-1] ".$ligne_prec[$j+$ind_debut-1]."\n";
			if ((($ligne_prec[$j+$ind_debut-1] != '.') and (!is_numeric($ligne_prec[$j+$ind_debut-1])))
			or (($ligne_prec[$j+$ind_debut+1] != '.') and (!is_numeric($ligne_prec[$j+$ind_debut+1])))
			or (($ligne[$j+$ind_debut-1] != '.') and (!is_numeric($ligne[$j+$ind_debut-1])))
			or (($ligne[$j+$ind_debut] != '.') and (!is_numeric($ligne[$j+$ind_debut])))
			or (($ligne[$j+$ind_debut+1] != '.') and (!is_numeric($ligne[$j+$ind_debut+1])))) {
				$symbole_adjacent = true ;
				echo "symbole adjacent\n";
			}
		} else {
		if ((($ligne_prec[$j+$ind_debut-1] != '.') and (!is_numeric($ligne_prec[$j+$ind_debut-1])))
			or (($ligne[$j+$ind_debut-1] != '.') and (!is_numeric($ligne[$j+$ind_debut-1])))
			or (($ligne[$j+$ind_debut] != '.') and (!is_numeric($ligne[$j+$ind_debut])))) {
				$symbole_adjacent = true ;
				echo "symbole adjacent\n";
		}
		}
		if ($symbole_adjacent) { 
			$resultat += intval($chiffre) ; 
			$symbole_adjacent = false ;
		}

	}
	$ind_debut = -1 ;
	$chiffre = '' ;
}

// cas général

//$symbole_adjacent = false ;

while ($ligne_suiv = str_split(fgets($fic))) {
	echo "dans while\n" ;
	if (is_numeric($ligne[0])) {
	// premier caractère
		$chiffre = $ligne[0] ;
		$ind_debut = 0 ;
	}
	for ($i=1 ; $i<$longueur ; $i++) {
		// recherche d'un chiffre dans la ligne
	echo "i $i ligne[i]".$ligne[$i]."\n";
		while (is_numeric($ligne[$i])) {
			$chiffre .= $ligne[$i] ;
			if ($ind_debut == -1) { $ind_debut = $i ; }
			if ($i<$longueur) { $i++ ; } else { break ;} 
		}
		if ($chiffre != '') {
		// vérifie si le chiffre est entouré de symbole
		if ($ind_debut == 0) {
			echo "CG dans if - ligne_prec[1]".$ligne_prec[1]." ligne[0] ".$ligne[0]." ligne[1] ".$ligne[1]."\n";
		//chiffre en début de ligne
			if ((($ligne_prec[1] != '.') and (!is_numeric($ligne_prec[1])))
			or (($ligne_prec[0] != '.') and (!is_numeric($ligne_prec[0])))
			or (($ligne[0] != '.') and (!is_numeric($ligne[0])))
			or (($ligne_suiv[0] != '.') and (!is_numeric($ligne_suiv[0])))
			or (($ligne_suiv[1] != '.') and (!is_numeric($ligne_suiv[1])))
			or (($ligne[1] != '.') and (!is_numeric($ligne[1])))) {
				echo "symbole adjacent\n";
				$symbole_adjacent = true ;
			}
		} else {
			echo "CG dans else - ligne_prec[1]".$ligne_prec[1]." ligne[0] ".$ligne[0]." ligne[1] ".$ligne[1]."\n";
			if ((($ligne_prec[$ind_debut-1] != '.') and (!is_numeric($ligne_prec[$ind_debut-1])))
			or (($ligne_prec[$ind_debut] != '.') and (!is_numeric($ligne_prec[$ind_debut])))
			or (($ligne_prec[$ind_debut+1] != '.') and (!is_numeric($ligne_prec[$ind_debut+1])))
			or (($ligne[$ind_debut-1] != '.') and (!is_numeric($ligne[$ind_debut-1])))
			or (($ligne_suiv[$ind_debut] != '.') and (!is_numeric($ligne_suiv[$ind_debut])))
			or (($ligne_suiv[$ind_debut-1] != '.') and (!is_numeric($ligne[$ind_debut-1])))
			or (($ligne_suiv[$ind_debut+1] != '.') and (!is_numeric($ligne_suiv[$ind_debut+1])))
			or (($ligne[$ind_debut+1] != '.') and (!is_numeric($ligne[$ind_debut+1])))) {
				$symbole_adjacent = true ;
				echo "symbole adjacent\n";
			}
		}
		if (contient_symbole($ligne_prec,$ligne,$ligne_suiv,$i, strlen($chiffre))) {
			$resultat += intval($chiffre) ;
			$symbole_adjacent = false ;
		}
		$ind_debut = -1 ;
		$chiffre = '' ;
		}
	}

	$ligne_prec = $ligne ;
	$ligne = $ligne_suiv ;
}
	if (!($ligne_suiv = str_split(fgets($fic)))) {
		echo "dernière ligne\n";
		// dernière ligne
		if (is_numeric($ligne[0])) {
			$chiffre = $ligne[0] ;
			$ind_debut = 0 ;
		}
		for ($i=1 ; $i<$longueur ; $i++) {
			while (is_numeric($ligne[$i])) {
				$chiffre .= $ligne[$i] ;
				if ($ind_debut == -1) { $ind_debut = $i ; }
				$i++ ;
			}
			if ($chiffre != '') {
				for($j=0 ; $j < strlen($chiffre) ; $j++) {
				if ((($ligne_prec[$j+$i-1] != '.') and (!is_numeric($ligne_prec[$j+$i-1])))
				or (($ligne_prec[$j+$i] != '.') and (!is_numeric($ligne_prec[$j+$i])))
				or (($ligne_prec[$j+$i+1] != '.') and (!is_numeric($ligne_prec[$j+$i+1])))
				or (($ligne[$j+$i-1] != '.') and (!is_numeric($ligne[$j+$i-1])))
				or (($ligne[$j+$i+1] != '.') and (!is_numeric($ligne[$j+$i+1])))) {
					$resultat += intval($chiffre) ;
					$symbole_adjacent = false ;
				}
				}
			}
			$ind_debut = -1 ;
			$chiffre = '' ;
		}
	}

fclose($fic);

echo "résultat " ;
echo $resultat ;
?>
