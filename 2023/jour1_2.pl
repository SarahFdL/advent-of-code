#!/usr/perl
use strict;
use warnings;

open(my $fic,'<',"input_1.txt") or die("ouverture : $!");

# entrée : tableau de cdc
# sortie : entier
#
# pour chaque ligne du fichier 
# regex 
# additionner les deux dans res -> multiplier le 1er par 10 et ajouter le 2e pour l'unité
# sortir res

my $res = 0 ;
	
sub calibration {
	my $ligne ;
	my %replace = (
		one => 'o1e',
		two => 't2o',
		three => 't3e',
		four => 'f4r',
		five => 'f5e',
		six => 's6x',
		seven => 's7n',
		eight => 'e8t',
		nine => 'n9n'
	);

	while (defined($ligne = <$fic>)) {
		chomp $ligne ;
		print "ligne originale $ligne\n" ;

		if ($ligne =~ s/(one|two|three|four|five|six|seven|eight|nine)/$replace{$1}/e) {
			print "ligne substituée : $ligne\n\$1 : $1\n";
			$ligne =~ s/(.*)(one|two|three|four|five|six|seven|eight|nine)/$replace{$2}/e ;
			print "ligne substituée 2 : $1 $ligne\n";
			$ligne = $1.$ligne ;
		}

		if ($ligne=~ m/\D*(\d).*(\d)\D*/) {
			print "deux chiffres $1 $2\n" ;
			print "\ndigit : " ;
			print $1*10 + $2 ;
			$res += $1*10 + $2 ;
		} else {
			$ligne=~ m/\D*(\d)\D*/ ;
			print "un seul chiffre $1\n" ;
			print "\ndigit : " ;
			print $1*10 + $1 ;
			$res += $1*10 + $1 ;
		}
		print "\nres : $res\n\n" ;
	}

	close ($fic);

	print "$res\n";
}

calibration() ;

