#!/usr/perl

use warnings ;
use strict ; 

open(my $fic,'<',"input_2.txt") or die("ouverture : $!");

sub colorgame {
	my $res = 0 ;
	my $possible = 1 ;
	my $id = 0 ;
	my $ligne ;
	my $tirages ;
	my @tirages ;
	my %tirages ;
	my $un_tirage ;
	my @un_tirage ;
	my %un_tirage ;
	my $dernier_tirage ;
	my @dernier_tirage ;
	my %dernier_tirage ;

	while (defined($ligne = <$fic>)) {
		chomp $ligne ;
		$possible = 1 ;
		# $1 = ID
		# $2 ensemble des tirages
		# $3 dernier tirage

		$ligne =~ m/^Game (\d+): (.*; )*(.*)$/ ;
		print "\n\n$ligne\n$1 \$2 $2 \$3 $3\n" ;
		$id = $1 ;
		$tirages = $2 ;
		$dernier_tirage = $3 ;

		$tirages =~ s/,//g ;
		@tirages = split(";",$tirages) ;
		foreach $un_tirage (@tirages) {
			@un_tirage = split(" ",$un_tirage) ;
			@un_tirage = reverse(@un_tirage) ;
			%un_tirage = @un_tirage ;
			foreach my $w (keys(%un_tirage)) {
				if ($w eq "green" and $un_tirage{$w} > 13) {
					$possible = 0 ;
				}
				if ($w eq "red" and $un_tirage{$w} > 12) {
					$possible = 0 ;
				}
				if ($w eq "blue" and $un_tirage{$w} > 14) {
					$possible = 0 ;
				}
			}
		}

		$dernier_tirage =~ s/,//g ;
		@dernier_tirage = split(" ",$dernier_tirage) ;
		@dernier_tirage = reverse(@dernier_tirage) ;
		%dernier_tirage = @dernier_tirage ;
		foreach my $v (keys(%dernier_tirage)) {
			if ($v eq "green" and $dernier_tirage{$v} > 13) {
				$possible = 0 ;
			}
			if ($v eq "red" and $dernier_tirage{$v} > 12) {
				$possible = 0 ;
			}
			if ($v eq "blue" and $dernier_tirage{$v} > 14) {
				$possible = 0 ;
			}
		}
		if ($possible != 0) { $res += $id ;}
	}

	print "\nrésultat : $res\n" ;
}

colorgame();

close($fic) ;
