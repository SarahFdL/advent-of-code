#!/usr/perl

use warnings ;
use strict ; 

open(my $fic,'<',"input_2.txt") or die("ouverture : $!");

sub colorgame {
	my $res = 0 ;
	my $ligne ;
	my $tirages ;
	my @tirages ;
	my %tirages ;
	my $un_tirage ;
	my @tous_tirages ;
	my %tous_tirages ;
	my $max_rouge = 0 ;
	my $max_vert = 0 ;
	my $max_bleu = 0 ;

	while (defined($ligne = <$fic>)) {
		chomp $ligne ;
		$tirages = '' ;
		@tous_tirages = () ;
		%tous_tirages = () ;
		# $2 ensemble des tirages
		# $3 dernier tirage

		$ligne =~ m/^Game \d+: (.*; )*(.*)$/ ;
		print "\n\n$ligne\n\n" ;
		$tirages .= $1.$2 ;

		print "\ntirages   : $tirages\n";
		$tirages =~ s/;/,/g ;
		@tirages = split(",",$tirages) ;
		my @tmp = () ;
		for (my $i = 0  ; $i < scalar @tirages ; $i++) {
			#print "$i : $tirages[$i]\n" ;
			@tmp = split(" ",$tirages[$i]) ;
			print "$i : $tmp[0] $tmp[1]\n" ;
			$tmp[1] = $tmp[1].$i ;
			push(@tous_tirages, reverse(@tmp)) ;
		}
		print "\nts tirages : @tous_tirages\n\n";

		%tous_tirages = @tous_tirages ;
 
		foreach $un_tirage (keys(%tous_tirages)) {
			print "\ncle=$un_tirage\n";
			print "valeur=$tous_tirages{$un_tirage}\n";
			if (($un_tirage =~ m/green.*/) and ($tous_tirages{$un_tirage} > $max_vert)) {
				$max_vert = $tous_tirages{$un_tirage} ;
				print "\ndans if vert $max_vert.\n" ;
			}
			if (($un_tirage =~ m/red.*/) and ($tous_tirages{$un_tirage} > $max_rouge)) {
				$max_rouge = $tous_tirages{$un_tirage} ;
				print "\ndans if rouge $max_rouge.\n" ;
			}
			if (($un_tirage =~ m/blue.*/) and ($tous_tirages{$un_tirage} > $max_bleu)) {
				$max_bleu = $tous_tirages{$un_tirage} ;
				print "\ndans if bleu $max_bleu\n" ;
			}
			#print "\nfin foreach bleu $max_bleu, rouge $max_rouge, vert $max_vert\n" ;
		}

		print "bleu $max_bleu, rouge $max_rouge, vert $max_vert. RES $res\n" ;
		$res += $max_vert*$max_bleu*$max_rouge ;
		print "\nrésultat tmp : $res\n" ;

		$max_rouge = 0 ;
		$max_vert = 0 ;
		$max_bleu = 0 ;
	}

	print "\nrésultat : $res\n" ;
}

colorgame();

close($fic) ;
