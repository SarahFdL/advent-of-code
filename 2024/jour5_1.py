f = open("jour5.txt", "r")
        
def enOrdre(n, i, tab):
#n est le 2e nb de la règle. Ne doit pas être avant i
#tab est une ligne d'ordre
    for j in range(i):
        if tab[j]==n:
            return False
    return True

rules=[]
orders=[]

for l in f:
    if not l.isspace():
        if l[2]=="|":
            rules.append(l.strip().split('|'))
        else:
            orders.append(l.strip().split(','))

regleOK=True
ordreOK=[]

for order in orders:
    for j in range(1,len(order)):
        for k in rules:
            if order[j]==k[0]:
                if not(enOrdre(k[1],j,order)):
                    regleOK=False
                    break
    if regleOK:
        ordreOK.append(order)
    else:
        regleOK = True

#calcul de la somme
somme=0
for i in ordreOK:
    somme+=int(i[len(i)//2])

print("somme ",somme)
