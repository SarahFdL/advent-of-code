f = open('jour4.txt', 'r')

arr=[]
nb_xmas=0

for l in f:
    l=l.strip()
    arr.append(list(l))

f.close()

# trouver XMAS
for i in range(len(arr)):
    for j in range(len(arr[i])):
        if(arr[i][j]=='X'):
            try:
                # mot horizontal GD
                if((arr[i][j+1]=='M') and (arr[i][j+2]=='A') and (arr[i][j+3]=='S')):
                    nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot horizontal DG
                # vérifier que l'indice n'est pas négatif
                if j-3 >=0 :
                    if((arr[i][j-1]=='M') and (arr[i][j-2]=='A') and (arr[i][j-3]=='S')):
                        nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot vertical HB
                if((arr[i+1][j]=='M') and (arr[i+2][j]=='A') and (arr[i+3][j]=='S')):
                    nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot vertical BH
                # vérifier que l'indice n'est pas négatif
                if i-3 >=0:
                    if((arr[i-1][j]=='M') and (arr[i-2][j]=='A') and (arr[i-3][j]=='S')):
                        nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot diagonal HG BD
                if((arr[i+1][j+1]=='M') and (arr[i+2][j+2]=='A') and (arr[i+3][j+3]=='S')):
                    nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot diagonal HD BG
                # vérifier que l'indice n'est pas négatif
                if j-3 >=0:
                    if((arr[i+1][j-1]=='M') and (arr[i+2][j-2]=='A') and (arr[i+3][j-3]=='S')):
                        nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot diagonal BG HD 
                # vérifier que l'indice n'est pas négatif
                if i-3 >=0:
                    if((arr[i-1][j+1]=='M') and (arr[i-2][j+2]=='A') and (arr[i-3][j+3]=='S')):
                        nb_xmas+=1
            except IndexError:
                pass
            try:
                # mot diagonal BD HG
                # vérifier que l'indice n'est pas négatif
                if i-3 >=0 and j-3 >=0:
                    if((arr[i-1][j-1]=='M') and (arr[i-2][j-2]=='A') and (arr[i-3][j-3]=='S')):
                        nb_xmas+=1
            except IndexError:
                pass

print("nb de XMAS : ",nb_xmas)
