f = open('jour2.txt', 'r')
somme=0

def is_safe(liste):
    prec=liste[0]
    if(int(liste[1])>int(prec)):
        inc=True
    else:
        inc=False 
    if((abs(int(liste[1])-int(prec)))>3) or (int(liste[1])==int(prec)):
        return False
    for i in range(1, len(liste)-1):
        prec=liste[i]
        if(inc and (int(liste[i+1])>int(prec))) or (not(inc) and (int(liste[i+1])<int(prec))):
            if((abs(int(liste[i+1])-int(prec)))>3):
                return False
        else:
            return False
    return True

for l in f:
    safe=0
    for i in range(len(l.split())):
        l_min = l.split()
        l_min.pop(i)
        if(is_safe(l_min)):
            safe=1
            break
        else:
            safe=0
    if is_safe(l.split()):
        safe=1
    somme+=safe

print(somme)
