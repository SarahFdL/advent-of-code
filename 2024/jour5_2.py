f = open("jour5.txt", "r")
        
def ligEnOrdre(lig,reg):
    for i in reg:
        if i[0] in lig and i[1] in lig:
            if lig.index(i[0]) > lig.index(i[1]):
                return False
    return True

def orgaLigne(lig,reg):
    while not(ligEnOrdre(lig,reg)):
        for i in reg:
            #si les deux éléments sont dans la ligne
            if i[0] in lig and i[1] in lig:
                if lig.index(i[0]) > lig.index(i[1]):
                    lig.remove(i[0])
                    lig.insert(lig.index(i[1]),i[0])
    return lig

# rangement dans les tableaux
rules=[]
orders=[]

for l in f:
    if not l.isspace():
        if l[2]=="|":
            rules.append(l.strip().split('|'))
        else:
            orders.append(l.strip().split(','))

ordreCorr=[]

for order in orders:
    if not(ligEnOrdre(order,rules)):
        ordreCorr.append(orgaLigne(order,rules))
    
#calcul de la somme
somme=0
for i in ordreCorr:
    somme+=int(i[len(i)//2])

print("somme ",somme)
