f = open("jour6.txt","r")

carte = []
nb_pos=0
direction=['H','D','B','G']

# remplissage tableau et recherche de la position initiale
for i, l in enumerate(f):
    if '^' in l:
        y = i
        x = l.index('^')
    carte.append(list(l.strip()))

# condition de fin
def fin(direct,x,y):
    if direct == 'H' and y==-1 or direct == 'B' and y==len(carte) or direct == 'G' and x == -1 or direct == 'D' and x==len(carte[0]):
        carte[y][x]='X'
        return True
    return False

# initialisation
d = direction[0]
y = y-1

while not(fin(d,x,y)):
    if carte[y][x] != '#':
        carte[y][x] = 'X'
        if d =='H':
          y=y-1
        elif d == 'B':
          y=y+1
        elif d == 'D':
          x = x+1
        else:
          x = x-1
    else:
        # changement de direction et correction de la prochaine case
        d=direction[(direction.index(d)+1)%4]
        if d == 'D':
            y=y+1
            x=x+1
        elif d == 'G':
            y=y-1
            x=x-1
        elif d == 'B':
            x=x-1
            y=y+1
        else:
            x=x+1
            y=y-1

for i in carte:
    print(i)
    nb_pos += i.count('X')
print('nb pos', nb_pos)

