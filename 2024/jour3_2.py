import re

#fichier en supprimant les retours chariots interprétés par Vim
f = open("jour3bis.txt", 'r')

res=[]
somme=0

# si on trouve un don't(), ne plus exécuter les instructions jusqu'à trouver un do()
# -> supprimer le contenu entre les deux

for l in f:
    l_sup = re.sub(r'don\'t\(\).*?do\(\)','',l)
    res = re.findall(r'mul\([0-9]{1,3},[0-9]{1,3}\)',l_sup)
    for i in res:
        mul = 0
        mul = i[4:-1].split(',')
        somme+=int(mul[0])*int(mul[1])
    res=[]
print("total : ",somme)

